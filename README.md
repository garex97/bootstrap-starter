# Bootstrap starter

### Build:

npm i (or npm install)


### Start server (php):

php -S localhost:2222
(eseguire nella cartella root del progetto)

### Useful links:

- https://css-tricks.com/bem-101/
- http://getbootstrap.com/docs/4.0/components/card/
- http://getbootstrap.com/docs/4.0/layout/overview/
- http://fontawesome.io/icons/


### Todo:
- Use webpack